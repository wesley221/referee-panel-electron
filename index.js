const {app, BrowserWindow, ipcMain} = require('electron');
const fs = require('fs');
const path = require('path');
const url = require('url');
const irc = require('irc');
const Store = require('electron-store');
const store = new Store();
const config = store.get('irc', {server: 'irc.ppy.sh', username: '', password: ''});

let mainWindow, 
    allGames = {},
    allErrors = [];

let ircClient = new irc.Client(config.server, config.username, {
    userName: config.username,
    realName: config.username,
    password: config.password,
    channels: []
});

// ==============
// Error handling
ircErrorHandler = (err) => {    
    allErrors.push({
        type: 'IRC error',
        error: JSON.stringify(err)
    });
};

ircClient.on('error', ircErrorHandler);

initialize = () => {
    // ============
    // Log messages
    ircClient.on('message', (nick, to, text, message) => {
        // ===================
        // The various RegExes
        const createTournamentMatchRegEx = new RegExp(/Created the tournament match https:\/\/osu.ppy.sh\/mp\/(\d+) ([0-9A-Za-z-_\[\] ]+): \(([0-9A-Za-z-_\[\] ]+)\) vs \(([0-9A-Za-z-_\[\] ]+)\)/).exec(text);
        const multiplayerChat            = new RegExp(/\#mp_([0-9]+)/);

        // ==========================================
        // Check if the message was sent by banchobot
        if(nick == "BanchoBot") {
            // ========================================
            // Check if the tournamentmatch was created
            if(createTournamentMatchRegEx) {
                const   multiplayerID   = createTournamentMatchRegEx[1],
                        acronym         = createTournamentMatchRegEx[2],
                        teamOne         = createTournamentMatchRegEx[3],
                        teamTwo         = createTournamentMatchRegEx[4];

                allGames[multiplayerID] = {
                    teamOneName         : teamOne,
                    teamTwoName         : teamTwo,
                    acronym             : acronym,
                    messageHistory      : [],
                    status              : 'open',
                    scoreTeamOne        : 0,
                    scoreTeamTwo        : 0,
                    scoreString         : `Current score: {{teamOneName}} : {{scoreTeamOne}} | {{scoreTeamTwo}} : {{teamTwoName}}`,
                    multiSlots          : 8,
                    teamMode            : 0, 
                    winCondition        : 0,
                    multiplayerSlots    : {
                        1: null,
                        2: null,
                        3: null,
                        4: null,
                        5: null,
                        6: null,
                        7: null,
                        8: null,
                        9: null,
                        10: null,
                        11: null,
                        12: null,
                        13: null,
                        14: null,
                        15: null,
                        16: null
                    }
                };

                mainWindow.reload();
            }

            // ===================================================
            // Check if the message was send in a multiplayer chat
            if(multiplayerChat.test(to)) {
                const multiplayerID = multiplayerChat.exec(to);

                // ===============
                // Various regexes
                const userJoined    = new RegExp(/([0-9A-Za-z-_\[\] ]+) joined in slot ([0-9]+)./).exec(text);
                const userLeft      = new RegExp(/([0-9A-Za-z-_\[\] ]+) left the game./).exec(text);
                const userMoved     = new RegExp(/([0-9A-Za-z-_\[\] ]+) moved to slot ([0-9]+)/).exec(text);

                // ===================================
                // A user joined the multiplayer lobby
                if(userJoined) {
                    allGames[multiplayerID[1]].multiplayerSlots[userJoined[2]] = userJoined[1];

                    mainWindow.webContents.send(`multiplayerPlayers${multiplayerID[1]}`, allGames[multiplayerID[1]]);
                }

                // =====================================
                // A user moved in the multiplayer lobby
                if(userMoved) {
                    for(let i in allGames[multiplayerID[1]].multiplayerSlots) {
                        if(allGames[multiplayerID[1]].multiplayerSlots[i] == userMoved[1]) {
                            allGames[multiplayerID[1]].multiplayerSlots[i] = null;
                        }
                    }

                    allGames[multiplayerID[1]].multiplayerSlots[userMoved[2]] = userMoved[1];

                    mainWindow.webContents.send(`multiplayerPlayers${multiplayerID[1]}`, allGames[multiplayerID[1]]);
                }

                // =================================
                // A user left the multiplayer lobby
                if(userLeft) {
                    for(let i in allGames[multiplayerID[1]].multiplayerSlots) {
                        if(allGames[multiplayerID[1]].multiplayerSlots[i] == userLeft[1]) {
                            allGames[multiplayerID[1]].multiplayerSlots[i] = null;
                        }
                    }

                    mainWindow.webContents.send(`multiplayerPlayers${multiplayerID[1]}`, allGames[multiplayerID[1]]);
                }

                // ======================================================
                // Check if BanchoBot says that the match has been closed
                if(text == 'Closed the match') {
                    allGames[multiplayerID[1]].status = 'closed';
                }
            }
        }

        // ======================
        // Message was sent to mp
        if(multiplayerChat.test(to)) {
            let dateObject = new Date(),
                curTime = dateObject.toLocaleTimeString();
            
            mainWindow.webContents.send(`multiChat${to}`, [curTime, nick, text]);

            allGames[to.replace('#mp_', '')].messageHistory.push([curTime, nick, text]);
        }
    });

    // ============
    // Socket stuff
    ipcMain.on('firstConnection', (event, args) => {
        event.sender.send('firstConnectionGames', allGames);
    });

    ipcMain.on('createLobby', (event, arg) => {
        ircClient.say('BanchoBot', `!mp make ${arg.acronym}: (${arg.teamOneName}) vs (${arg.teamTwoName})`);
    });

    ipcMain.on('sendMessage', (event, args) => {
        let dateObject = new Date(),
            curTime = dateObject.toLocaleTimeString();

        if(allGames[args[0]].status != 'open') {
            event.sender.send(`multiChat#mp_${args[0]}`, [curTime, 'ERROR', `This mutliplayer lobby has been closed already.`]);
        }
        else {
            ircClient.say(`#mp_${args[0]}`, args[1]);
            event.sender.send(`multiChat#mp_${args[0]}`, [curTime, 'ME', args[1]]);
            allGames[args[0]].messageHistory.push([curTime, 'ME', args[1]]);
        }
    });
}

app.on('ready', () => {
    mainWindow = new BrowserWindow({icon: __dirname + '/web/resources/favicon.png'}); 

    mainWindow.on('close', () => {
        mainWindow = null;
        app.quit();
    });

    if(config.server == '' || config.username == '' || config.password == '') {
        mainWindow.loadURL(url.format({
            pathname: path.join(__dirname, 'web/irc.html'),
            protocol: 'file:',
            slashes: true
        }));

        mainWindow.setSize(540, 370);
    }
    else {
        mainWindow.loadURL(url.format({
            pathname: path.join(__dirname, 'web/index.html'),
            protocol: 'file:',
            slashes: true
        }));

        mainWindow.maximize();

        initialize();
    }
});

app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
        app.quit();
    }
});

app.on('activate', () => {
    if (mainWindow === null) {
        initialize();
    }
});

ipcMain.on('requestConfig', (event, arg) => {
    mainWindow.webContents.send('receiveConfig', config);
});

ipcMain.on('saveIrcCredentials', (event, arg) => {
    const irc = {
        server: arg[0],
        username: arg[1],
        password: arg[2]
    };

    store.set('irc', irc);
    app.quit();
});

ipcMain.on('deleteIrcCredentials', (event, arg) => {
    store.delete('irc');
    app.quit();
});

ipcMain.on('changeGameStatus', (event, arg) => {
    allGames[arg[0]].teamMode       = arg[1];
    allGames[arg[0]].winCondition   = arg[2];
    allGames[arg[0]].multiSlots     = arg[3];

    allGames[arg[0]].scoreTeamOne   = arg[4];
    allGames[arg[0]].scoreTeamTwo   = arg[5];
});

ipcMain.on('changeScoreString', (event, arg) => {
    allGames[arg[0]].scoreString = arg[1];
});

ipcMain.on('requestErrors', (event, arg) => {
    mainWindow.webContents.send('receiveErrors', allErrors);
});

// ==================================================
// Prevent the app from crashing when an error occurs
process.on("uncaughtException", err => {
    const errorMsg = err.stack.replace(new RegExp(`${__dirname}/`, "g"), "./");
    console.error("Uncaught Exception: ", errorMsg);
    
    allErrors.push({
        type: 'uncaughtException',
        error: JSON.stringify(err)
    });
});

process.on("unhandledRejection", err => {
    const errorMsg = err.stack.replace(new RegExp(`${__dirname}/`, "g"), "./");
    console.error("Uncaught Promise Error: ", errorMsg);

    allErrors.push({
        type: 'unhandledRejection',
        error: JSON.stringify(err)
    });
});