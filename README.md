To use the interface, download the file `referee-interface-win32-x64.rar` and extract it. Then open the folder, and open the file `referee-interface.exe` and you're good to go!

To reset your password go to the `Settings` tab to forget the IRC credentials.

I've used the following module to package the files: https://github.com/electron-userland/electron-packager in case you want to package it yourself :)

![](https://i.imgur.com/IWSyFOZ.png)
![](https://i.imgur.com/AmpTjYq.png)
![](https://i.imgur.com/H7qVdRL.png)
![](https://i.imgur.com/yTI8gfZ.png)